import requests
import re

# Configuring a request with URL and valid headers
URL = "https://the-internet.herokuapp.com/context_menu"
HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36 Edg/96.0.1054.53'
}


# searches the string for a match, and returns a Match object if there is a match, else return None.
def find_text_in_html(word):
    page = requests.get(URL, HEADERS)
    html_text = page.text
    return re.search(word, html_text)


# Test should PASS if the string “Right-click in the box below to see one called 'the-internet' “ found on page
def test_right_click_text():
    Right_Click = "Right-click in the box below to see one called 'the-internet'"
    assert find_text_in_html(Right_Click)


# Test should FAIL if string “Alibaba” is not found on the same page
def test_alibaba_text():
    Alibaba = "Alibaba"
    assert find_text_in_html(Alibaba)



